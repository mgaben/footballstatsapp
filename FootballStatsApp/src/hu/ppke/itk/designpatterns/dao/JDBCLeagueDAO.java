package hu.ppke.itk.designpatterns.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import hu.ppke.itk.designpatterns.Team;
import hu.ppke.itk.designpatterns.business.LeagueSeasonStat;


public class JDBCLeagueDAO implements LeagueDAO {
	
	Connection conn = null;
	List<String> leagues = new ArrayList<>();
	
	public Connection connect() {
        try {
        		Class.forName("org.sqlite.JDBC");
        		String db = "jdbc:sqlite:database.sqlite";
            // create a connection to the database
            if(conn == null) {
            		conn = DriverManager.getConnection(db);
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
        return conn;
    }
	
	@Override
	public List<String> selectLeagueNames() {
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM League");
			
			while(resultSet.next()) {
				leagues.add(resultSet.getString("name"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return leagues;
	}
	
	public void closeConnection(){
		 try {
			 if (conn != null) {
				 conn.close();
			 }
		 } catch (Exception e) { }
	}
	
	@Override
	public int selectLeagueIdByName(String name) {
		int id = -1;
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM League WHERE name=\"" + name + "\"");
			
			while(resultSet.next()) {
				id = resultSet.getInt("country_id");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	@Override
	public List<LeagueSeasonStat> selectLeagueStat() {
		List<LeagueSeasonStat> seasonStats = new ArrayList<>();
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT Country.name AS country_name, \n" + 
					"                                        League.name AS league_name, \n" + 
					"                                        season,\n" + 
					"                                        count(distinct stage) AS number_of_stages,\n" + 
					"                                        count(distinct HT.team_long_name) AS number_of_teams,\n" + 
					"                                        avg(home_team_goal) AS avg_home_team_goals, \n" + 
					"                                        avg(away_team_goal) AS avg_away_team_goals, \n" + 
					"                                        avg(home_team_goal-away_team_goal) AS avg_goal_diff, \n" + 
					"                                        avg(home_team_goal+away_team_goal) AS avg_goals, \n" + 
					"                                        sum(home_team_goal+away_team_goal) AS total_goals \n" + 
					"                                FROM Match\n" + 
					"                                JOIN Country on Country.id = Match.country_id\n" + 
					"                                JOIN League on League.id = Match.league_id\n" + 
					"                                LEFT JOIN Team AS HT on HT.team_api_id = Match.home_team_api_id\n" + 
					"                                LEFT JOIN Team AS AT on AT.team_api_id = Match.away_team_api_id\n" + 
					"                                WHERE country_name in ('Spain', 'Germany', 'France', 'Italy', 'England')\n" + 
					"                                GROUP BY Country.name, League.name, season\n" + 
					"                                HAVING count(distinct stage) > 10\n" + 
					"                                ORDER BY Country.name, League.name, season DESC\n" + 
					"                                ;");
			
			while(resultSet.next()) {
				String country = resultSet.getString("country_name");
				String league = resultSet.getString("league_name");
				String season = resultSet.getString("season");
				int stages = resultSet.getInt("number_of_stages");
				int numOfTeams = resultSet.getInt("number_of_teams");
				int sumGoals = resultSet.getInt("total_goals");
				float homeTeamGoals = resultSet.getFloat("avg_home_team_goals");
				float awayTeamGoals = resultSet.getFloat("avg_away_team_goals");
				float avgGoalDiff = resultSet.getFloat("avg_goal_diff");
				float avgGoals = resultSet.getFloat("avg_goals");
				
				LeagueSeasonStat stat = new LeagueSeasonStat(country, league, season, stages, numOfTeams, sumGoals, homeTeamGoals, awayTeamGoals, avgGoalDiff, avgGoals);
				seasonStats.add(stat);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return seasonStats;
	}
}
