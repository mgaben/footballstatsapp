package hu.ppke.itk.designpatterns.dao;

import java.util.List;

import hu.ppke.itk.designpatterns.Match;

public interface MatchDAO {
	public List<Match> select();
	public List<String> selectSeasons();
	public List<Match> selectByTeams(String team1, String team2);
	public List<Match> selectByOpponentsId(int id1, int id2);
	public List<Match> selectByLeagueId(int id);
}
