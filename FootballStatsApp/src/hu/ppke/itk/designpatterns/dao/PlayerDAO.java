package hu.ppke.itk.designpatterns.dao;

import java.util.List;

import hu.ppke.itk.designpatterns.Player;

public interface PlayerDAO {
    public List<Player> select();
    public Player selectByID(int id);
    public List<String> selectAllNames();
}
