package hu.ppke.itk.designpatterns.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hu.ppke.itk.designpatterns.Match;
import hu.ppke.itk.designpatterns.Player;
import hu.ppke.itk.designpatterns.Team;

public class JDBCMatchDAO implements MatchDAO {
	
	Connection conn = null;
	List<Match> matches = new ArrayList<>();
	
	public Connection connect() {
        try {
        		Class.forName("org.sqlite.JDBC");
        		String db = "jdbc:sqlite:database.sqlite";
            // create a connection to the database
            if(conn == null) {
            		conn = DriverManager.getConnection(db);
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
        return conn;
    }
	
	@Override
	public List<Match> selectByOpponentsId(int id1, int id2) {
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM match WHERE (home_team_api_id=" + String.valueOf(id1) + " AND away_team_api_id="  + String.valueOf(id2) + ") OR (home_team_api_id="  + String.valueOf(id1) + " AND away_team_api_id="  + String.valueOf(id1) + ")");
			
			while(resultSet.next()) {
				Match m = null;
				
				// get country name
				int country_id = resultSet.getInt("country_id");
				Statement country_statement = conn.createStatement();
				ResultSet countrySet = country_statement.executeQuery("SELECT name FROM Country WHERE id=" + country_id);
				String country = countrySet.getString("name");
				
				// get league name
				int league_id = resultSet.getInt("league_id");
				Statement league_statement = conn.createStatement();
				ResultSet leagueSet = league_statement.executeQuery("SELECT name FROM League WHERE id=" + league_id);
				String league = leagueSet.getString("name");
				
				// get season
				String season = resultSet.getString("season");
				// get stage
				int stage = resultSet.getInt("stage");
				// get date
				String dateString = resultSet.getString("date");
				DateFormat format = new SimpleDateFormat("YYYY-MM-DD 00:00:00");
				Date date = null;
				try {
					date = format.parse(dateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				// get teams by their ids
				JDBCTeamDAO jdbcTeamDAO = new JDBCTeamDAO();
		        jdbcTeamDAO.connect(); 
		        Team home_team = jdbcTeamDAO.selectByID(resultSet.getInt("home_team_api_id"));
		        Team away_team = jdbcTeamDAO.selectByID(resultSet.getInt("away_team_api_id"));
		        jdbcTeamDAO.closeConnection();
		        
		        // get goals
		        int home_team_goal = resultSet.getInt("home_team_goal");
		        int away_team_goal = resultSet.getInt("away_team_goal");
		        
		        // get players
		        JDBCPlayerDAO jdbcPlayerDAO = new JDBCPlayerDAO();
		        jdbcPlayerDAO.connect();
		        	
		        List<Integer> home_player_X = new ArrayList<>();
		    		List<Integer> home_player_Y = new ArrayList<>();
		    		List<Integer> away_player_X = new ArrayList<>();
		    		List<Integer> away_player_Y = new ArrayList<>();
		        List<Player> home_players = new ArrayList<>();
		    		List<Player> away_players = new ArrayList<>();
		        for(int i=1; i<=11; ++i) {
		        		// get X positions of home players
		        		Integer hxi = resultSet.getInt("home_player_X" + String.valueOf(i));
	        			home_player_X.add(hxi);
			        // get Y positions of home players
	        			Integer hyi = resultSet.getInt("home_player_Y" + String.valueOf(i));
	        			home_player_Y.add(hyi);
			        // get X positions of away players
	        			Integer axi = resultSet.getInt("away_player_X" + String.valueOf(i));
	        			away_player_X.add(axi);
		        		// get Y positions of away players
	        			Integer ayi = resultSet.getInt("away_player_Y" + String.valueOf(i));
        				away_player_Y.add(ayi);
        				// add players
		        		home_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("home_player_" + String.valueOf(i))));
		        		away_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("away_player_" + String.valueOf(i))));
		        }
		        
		        jdbcTeamDAO.closeConnection();
		        
		        // get stats of match
		        String goal = resultSet.getString("goal");
		        String shoton = resultSet.getString("shoton");
		        	String shotoff = resultSet.getString("shotoff");
		        	String foulcommit = resultSet.getString("foulcommit");
		        	String card = resultSet.getString("card");
		        	String cross = resultSet.getString("cross");
		        	String corner = resultSet.getString("corner");
		        	String possession = resultSet.getString("possession");
		        
		        // get odds of match
		        Float b365h = resultSet.getFloat("B365H");
		        Float b365d = resultSet.getFloat("B365D");
		        Float b365a = resultSet.getFloat("B365A");
		        Float bWH = resultSet.getFloat("BWH");
		        Float bWD = resultSet.getFloat("BWD");
		        Float bWA = resultSet.getFloat("BWA");
		        Float iWH = resultSet.getFloat("IWH");
		        Float iWD = resultSet.getFloat("IWD");
		        Float iWA = resultSet.getFloat("IWA");
		        Float lBH = resultSet.getFloat("LBH");
		        Float lBD = resultSet.getFloat("LBD");
		        Float lBA = resultSet.getFloat("LBA");
		        Float pSH = resultSet.getFloat("PSH");
		        Float pSD = resultSet.getFloat("PSD");
		        Float pSA = resultSet.getFloat("PSA");
		        Float wHH =  resultSet.getFloat("WHH");
				Float wHD = resultSet.getFloat("WHD");
				Float wHA = resultSet.getFloat("WHA");
				Float sJH = resultSet.getFloat("SJH");
				Float sJD = resultSet.getFloat("SJD");
				Float sJA = resultSet.getFloat("SJA");
				Float vCH = resultSet.getFloat("VCH");
				Float vCD = resultSet.getFloat("VCD");
				Float vCA = resultSet.getFloat("VCA");
				Float gBH = resultSet.getFloat("GBH");
				Float gBD = resultSet.getFloat("GBD");
				Float gBA = resultSet.getFloat("GBA");
				Float bSH = resultSet.getFloat("BSH");
				Float bSD = resultSet.getFloat("BSD");
				Float bSA = resultSet.getFloat("BSA");
				
				m = new Match(country, league, season, stage, date, home_team, away_team, 
						home_team_goal, away_team_goal, home_player_X, home_player_Y, 
						away_player_X, away_player_Y, home_players, away_players, 
						goal, shoton, shotoff, foulcommit, card, cross, corner, possession, 
						b365h, b365d, b365a, bWH, bWD, bWA, iWH, iWD, iWA, lBH, lBD, lBA, pSH, 
						pSD, pSA, wHH, wHD, wHA, sJH, sJD, sJA, vCH, vCD, vCA, gBH, gBD, gBA, bSH, bSD, bSA);
				
				matches.add(m);
			}
			
            resultSet.close();
            statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return matches;
	}

	@Override
	public List<Match> select() {
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Match");
			
			while(resultSet.next()) {
				Match m = null;
				
				// get country name
				int country_id = resultSet.getInt("country_id");
				Statement country_statement = conn.createStatement();
				ResultSet countrySet = country_statement.executeQuery("SELECT name FROM Country WHERE id=" + country_id);
				String country = countrySet.getString("name");
				
				// get league name
				int league_id = resultSet.getInt("league_id");
				Statement league_statement = conn.createStatement();
				ResultSet leagueSet = league_statement.executeQuery("SELECT name FROM League WHERE id=" + league_id);
				String league = leagueSet.getString("name");
				
				// get season
				String season = resultSet.getString("season");
				// get stage
				int stage = resultSet.getInt("stage");
				// get date
				String dateString = resultSet.getString("date");
				DateFormat format = new SimpleDateFormat("YYYY-MM-DD 00:00:00");
				Date date = null;
				try {
					date = format.parse(dateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				// get teams by their ids
				JDBCTeamDAO jdbcTeamDAO = new JDBCTeamDAO();
		        jdbcTeamDAO.connect(); 
		        Team home_team = jdbcTeamDAO.selectByID(resultSet.getInt("home_team_api_id"));
		        Team away_team = jdbcTeamDAO.selectByID(resultSet.getInt("away_team_api_id"));
		        jdbcTeamDAO.closeConnection();
		        
		        // get goals
		        int home_team_goal = resultSet.getInt("home_team_goal");
		        int away_team_goal = resultSet.getInt("away_team_goal");
		        
		        // get players
		        JDBCPlayerDAO jdbcPlayerDAO = new JDBCPlayerDAO();
		        jdbcPlayerDAO.connect();
		        	
		        List<Integer> home_player_X = new ArrayList<>();
		    		List<Integer> home_player_Y = new ArrayList<>();
		    		List<Integer> away_player_X = new ArrayList<>();
		    		List<Integer> away_player_Y = new ArrayList<>();
		        List<Player> home_players = new ArrayList<>();
		    		List<Player> away_players = new ArrayList<>();
		        for(int i=1; i<=11; ++i) {
		        		// get X positions of home players
		        		Integer hxi = resultSet.getInt("home_player_X" + String.valueOf(i));
	        			home_player_X.add(hxi);
			        // get Y positions of home players
	        			Integer hyi = resultSet.getInt("home_player_Y" + String.valueOf(i));
	        			home_player_Y.add(hyi);
			        // get X positions of away players
	        			Integer axi = resultSet.getInt("away_player_X" + String.valueOf(i));
	        			away_player_X.add(axi);
		        		// get Y positions of away players
	        			Integer ayi = resultSet.getInt("away_player_Y" + String.valueOf(i));
        				away_player_Y.add(ayi);
        				// add players
		        		home_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("home_player_" + String.valueOf(i))));
		        		away_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("away_player_" + String.valueOf(i))));
		        }
		        
		        jdbcTeamDAO.closeConnection();
		        
		        // get stats of match
		        String goal = resultSet.getString("goal");
		        String shoton = resultSet.getString("shoton");
		        	String shotoff = resultSet.getString("shotoff");
		        	String foulcommit = resultSet.getString("foulcommit");
		        	String card = resultSet.getString("card");
		        	String cross = resultSet.getString("cross");
		        	String corner = resultSet.getString("corner");
		        	String possession = resultSet.getString("possession");
		        
		        // get odds of match
		        Float b365h = resultSet.getFloat("B365H");
		        Float b365d = resultSet.getFloat("B365D");
		        Float b365a = resultSet.getFloat("B365A");
		        Float bWH = resultSet.getFloat("BWH");
		        Float bWD = resultSet.getFloat("BWD");
		        Float bWA = resultSet.getFloat("BWA");
		        Float iWH = resultSet.getFloat("IWH");
		        Float iWD = resultSet.getFloat("IWD");
		        Float iWA = resultSet.getFloat("IWA");
		        Float lBH = resultSet.getFloat("LBH");
		        Float lBD = resultSet.getFloat("LBD");
		        Float lBA = resultSet.getFloat("LBA");
		        Float pSH = resultSet.getFloat("PSH");
		        Float pSD = resultSet.getFloat("PSD");
		        Float pSA = resultSet.getFloat("PSA");
		        Float wHH =  resultSet.getFloat("WHH");
				Float wHD = resultSet.getFloat("WHD");
				Float wHA = resultSet.getFloat("WHA");
				Float sJH = resultSet.getFloat("SJH");
				Float sJD = resultSet.getFloat("SJD");
				Float sJA = resultSet.getFloat("SJA");
				Float vCH = resultSet.getFloat("VCH");
				Float vCD = resultSet.getFloat("VCD");
				Float vCA = resultSet.getFloat("VCA");
				Float gBH = resultSet.getFloat("GBH");
				Float gBD = resultSet.getFloat("GBD");
				Float gBA = resultSet.getFloat("GBA");
				Float bSH = resultSet.getFloat("BSH");
				Float bSD = resultSet.getFloat("BSD");
				Float bSA = resultSet.getFloat("BSA");
				
				m = new Match(country, league, season, stage, date, home_team, away_team, 
						home_team_goal, away_team_goal, home_player_X, home_player_Y, 
						away_player_X, away_player_Y, home_players, away_players, 
						goal, shoton, shotoff, foulcommit, card, cross, corner, possession, 
						b365h, b365d, b365a, bWH, bWD, bWA, iWH, iWD, iWA, lBH, lBD, lBA, pSH, 
						pSD, pSA, wHH, wHD, wHA, sJH, sJD, sJA, vCH, vCD, vCA, gBH, gBD, gBA, bSH, bSD, bSA);
				
				matches.add(m);
			}
			
            resultSet.close();
            statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return matches;
	}
	
	@Override
	public List<Match> selectByLeagueId(int id) {
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM match WHERE league_id=" + id);
			
			while(resultSet.next()) {
				Match m = null;
				
				// get country name
				int country_id = resultSet.getInt("country_id");
				Statement country_statement = conn.createStatement();
				ResultSet countrySet = country_statement.executeQuery("SELECT name FROM Country WHERE id=" + country_id);
				String country = countrySet.getString("name");
				
				// get league name
				int league_id = resultSet.getInt("league_id");
				Statement league_statement = conn.createStatement();
				ResultSet leagueSet = league_statement.executeQuery("SELECT name FROM League WHERE id=" + league_id);
				String league = leagueSet.getString("name");
				
				// get season
				String season = resultSet.getString("season");
				// get stage
				int stage = resultSet.getInt("stage");
				// get date
				String dateString = resultSet.getString("date");
				DateFormat format = new SimpleDateFormat("YYYY-MM-DD 00:00:00");
				Date date = null;
				try {
					date = format.parse(dateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				// get teams by their ids
				JDBCTeamDAO jdbcTeamDAO = new JDBCTeamDAO();
		        jdbcTeamDAO.connect(); 
		        Team home_team = jdbcTeamDAO.selectByID(resultSet.getInt("home_team_api_id"));
		        Team away_team = jdbcTeamDAO.selectByID(resultSet.getInt("away_team_api_id"));
		        jdbcTeamDAO.closeConnection();
		        
		        // get goals
		        int home_team_goal = resultSet.getInt("home_team_goal");
		        int away_team_goal = resultSet.getInt("away_team_goal");
		        
		        // get players
		        JDBCPlayerDAO jdbcPlayerDAO = new JDBCPlayerDAO();
		        jdbcPlayerDAO.connect();
		        	
		        List<Integer> home_player_X = new ArrayList<>();
		    		List<Integer> home_player_Y = new ArrayList<>();
		    		List<Integer> away_player_X = new ArrayList<>();
		    		List<Integer> away_player_Y = new ArrayList<>();
		        List<Player> home_players = new ArrayList<>();
		    		List<Player> away_players = new ArrayList<>();
		        for(int i=1; i<=11; ++i) {
		        		// get X positions of home players
		        		Integer hxi = resultSet.getInt("home_player_X" + String.valueOf(i));
	        			home_player_X.add(hxi);
			        // get Y positions of home players
	        			Integer hyi = resultSet.getInt("home_player_Y" + String.valueOf(i));
	        			home_player_Y.add(hyi);
			        // get X positions of away players
	        			Integer axi = resultSet.getInt("away_player_X" + String.valueOf(i));
	        			away_player_X.add(axi);
		        		// get Y positions of away players
	        			Integer ayi = resultSet.getInt("away_player_Y" + String.valueOf(i));
        				away_player_Y.add(ayi);
        				// add players
		        		home_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("home_player_" + String.valueOf(i))));
		        		away_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("away_player_" + String.valueOf(i))));
		        }
		        
		        jdbcTeamDAO.closeConnection();
		        
		        // get stats of match
		        String goal = resultSet.getString("goal");
		        String shoton = resultSet.getString("shoton");
		        	String shotoff = resultSet.getString("shotoff");
		        	String foulcommit = resultSet.getString("foulcommit");
		        	String card = resultSet.getString("card");
		        	String cross = resultSet.getString("cross");
		        	String corner = resultSet.getString("corner");
		        	String possession = resultSet.getString("possession");
		        
		        // get odds of match
		        Float b365h = resultSet.getFloat("B365H");
		        Float b365d = resultSet.getFloat("B365D");
		        Float b365a = resultSet.getFloat("B365A");
		        Float bWH = resultSet.getFloat("BWH");
		        Float bWD = resultSet.getFloat("BWD");
		        Float bWA = resultSet.getFloat("BWA");
		        Float iWH = resultSet.getFloat("IWH");
		        Float iWD = resultSet.getFloat("IWD");
		        Float iWA = resultSet.getFloat("IWA");
		        Float lBH = resultSet.getFloat("LBH");
		        Float lBD = resultSet.getFloat("LBD");
		        Float lBA = resultSet.getFloat("LBA");
		        Float pSH = resultSet.getFloat("PSH");
		        Float pSD = resultSet.getFloat("PSD");
		        Float pSA = resultSet.getFloat("PSA");
		        Float wHH =  resultSet.getFloat("WHH");
				Float wHD = resultSet.getFloat("WHD");
				Float wHA = resultSet.getFloat("WHA");
				Float sJH = resultSet.getFloat("SJH");
				Float sJD = resultSet.getFloat("SJD");
				Float sJA = resultSet.getFloat("SJA");
				Float vCH = resultSet.getFloat("VCH");
				Float vCD = resultSet.getFloat("VCD");
				Float vCA = resultSet.getFloat("VCA");
				Float gBH = resultSet.getFloat("GBH");
				Float gBD = resultSet.getFloat("GBD");
				Float gBA = resultSet.getFloat("GBA");
				Float bSH = resultSet.getFloat("BSH");
				Float bSD = resultSet.getFloat("BSD");
				Float bSA = resultSet.getFloat("BSA");
				
				m = new Match(country, league, season, stage, date, home_team, away_team, 
						home_team_goal, away_team_goal, home_player_X, home_player_Y, 
						away_player_X, away_player_Y, home_players, away_players, 
						goal, shoton, shotoff, foulcommit, card, cross, corner, possession, 
						b365h, b365d, b365a, bWH, bWD, bWA, iWH, iWD, iWA, lBH, lBD, lBA, pSH, 
						pSD, pSA, wHH, wHD, wHA, sJH, sJD, sJA, vCH, vCD, vCA, gBH, gBD, gBA, bSH, bSD, bSA);
				
				matches.add(m);
			}
			
            resultSet.close();
            statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return matches;
	}
	
	@Override
	public List<String> selectSeasons() {
		List<String> seasons = new ArrayList<>();
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT DISTINCT season FROM Match");
			
			while(resultSet.next()) {
				seasons.add(resultSet.getString("season"));
			}
			
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return seasons;
	}
	
	public void closeConnection(){
		try {
			if (conn != null) {
			conn.close();
			}
		} catch (Exception e) { }
	}
	  
	@Override
	public List<Match> selectByTeams(String team1, String team2) {
		List<Match> team_matches = new ArrayList<>();
		
		try {
			JDBCTeamDAO teamDAO = new JDBCTeamDAO();
			teamDAO.connect();
			int teamId_1 = teamDAO.selectByName(team1).getId();
			int teamId_2 = teamDAO.selectByName(team2).getId();
			
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM match WHERE home_team_api_id=" + teamId_1 + " AND away_team_api_id=" + teamId_2);
			
			while(resultSet.next()) {
				Match m = null;
				
				// get country name
				int country_id = resultSet.getInt("country_id");
				Statement country_statement = conn.createStatement();
				ResultSet countrySet = country_statement.executeQuery("SELECT name FROM Country WHERE id=" + country_id);
				String country = countrySet.getString("name");
				
				// get league name
				int league_id = resultSet.getInt("league_id");
				Statement league_statement = conn.createStatement();
				ResultSet leagueSet = league_statement.executeQuery("SELECT name FROM League WHERE id=" + league_id);
				String league = leagueSet.getString("name");
				
				// get season
				String season = resultSet.getString("season");
				// get stage
				int stage = resultSet.getInt("stage");
				// get date
				String dateString = resultSet.getString("date");
				DateFormat format = new SimpleDateFormat("YYYY-MM-DD 00:00:00");
				Date date = null;
				try {
					date = format.parse(dateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				// get teams by their ids
				JDBCTeamDAO jdbcTeamDAO = new JDBCTeamDAO();
		        jdbcTeamDAO.connect(); 
		        Team home_team = jdbcTeamDAO.selectByID(resultSet.getInt("home_team_api_id"));
		        Team away_team = jdbcTeamDAO.selectByID(resultSet.getInt("away_team_api_id"));
		        jdbcTeamDAO.closeConnection();
		        
		        // get goals
		        int home_team_goal = resultSet.getInt("home_team_goal");
		        int away_team_goal = resultSet.getInt("away_team_goal");
		        
		        // get players
		        JDBCPlayerDAO jdbcPlayerDAO = new JDBCPlayerDAO();
		        jdbcPlayerDAO.connect();
		        	
		        List<Integer> home_player_X = new ArrayList<>();
		    		List<Integer> home_player_Y = new ArrayList<>();
		    		List<Integer> away_player_X = new ArrayList<>();
		    		List<Integer> away_player_Y = new ArrayList<>();
		        List<Player> home_players = new ArrayList<>();
		    		List<Player> away_players = new ArrayList<>();
		        for(int i=1; i<=11; ++i) {
		        		// get X positions of home players
		        		Integer hxi = resultSet.getInt("home_player_X" + String.valueOf(i));
	        			home_player_X.add(hxi);
			        // get Y positions of home players
	        			Integer hyi = resultSet.getInt("home_player_Y" + String.valueOf(i));
	        			home_player_Y.add(hyi);
			        // get X positions of away players
	        			Integer axi = resultSet.getInt("away_player_X" + String.valueOf(i));
	        			away_player_X.add(axi);
		        		// get Y positions of away players
	        			Integer ayi = resultSet.getInt("away_player_Y" + String.valueOf(i));
        				away_player_Y.add(ayi);
        				// add players
		        		home_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("home_player_" + String.valueOf(i))));
		        		away_players.add(jdbcPlayerDAO.selectByID(resultSet.getInt("away_player_" + String.valueOf(i))));
		        }
		        		        
		        // get stats of match
		        String goal = resultSet.getString("goal");
		        String shoton = resultSet.getString("shoton");
		        	String shotoff = resultSet.getString("shotoff");
		        	String foulcommit = resultSet.getString("foulcommit");
		        	String card = resultSet.getString("card");
		        	String cross = resultSet.getString("cross");
		        	String corner = resultSet.getString("corner");
		        	String possession = resultSet.getString("possession");
		        
		        // get odds of match
		        Float b365h = resultSet.getFloat("B365H");
		        Float b365d = resultSet.getFloat("B365D");
		        Float b365a = resultSet.getFloat("B365A");
		        Float bWH = resultSet.getFloat("BWH");
		        Float bWD = resultSet.getFloat("BWD");
		        Float bWA = resultSet.getFloat("BWA");
		        Float iWH = resultSet.getFloat("IWH");
		        Float iWD = resultSet.getFloat("IWD");
		        Float iWA = resultSet.getFloat("IWA");
		        Float lBH = resultSet.getFloat("LBH");
		        Float lBD = resultSet.getFloat("LBD");
		        Float lBA = resultSet.getFloat("LBA");
		        Float pSH = resultSet.getFloat("PSH");
		        Float pSD = resultSet.getFloat("PSD");
		        Float pSA = resultSet.getFloat("PSA");
		        Float wHH =  resultSet.getFloat("WHH");
				Float wHD = resultSet.getFloat("WHD");
				Float wHA = resultSet.getFloat("WHA");
				Float sJH = resultSet.getFloat("SJH");
				Float sJD = resultSet.getFloat("SJD");
				Float sJA = resultSet.getFloat("SJA");
				Float vCH = resultSet.getFloat("VCH");
				Float vCD = resultSet.getFloat("VCD");
				Float vCA = resultSet.getFloat("VCA");
				Float gBH = resultSet.getFloat("GBH");
				Float gBD = resultSet.getFloat("GBD");
				Float gBA = resultSet.getFloat("GBA");
				Float bSH = resultSet.getFloat("BSH");
				Float bSD = resultSet.getFloat("BSD");
				Float bSA = resultSet.getFloat("BSA");
				
				m = new Match(country, league, season, stage, date, home_team, away_team, 
						home_team_goal, away_team_goal, home_player_X, home_player_Y, 
						away_player_X, away_player_Y, home_players, away_players, 
						goal, shoton, shotoff, foulcommit, card, cross, corner, possession, 
						b365h, b365d, b365a, bWH, bWD, bWA, iWH, iWD, iWA, lBH, lBD, lBA, pSH, 
						pSD, pSA, wHH, wHD, wHA, sJH, sJD, sJA, vCH, vCD, vCA, gBH, gBD, gBA, bSH, bSD, bSA);
				
				team_matches.add(m);
			}
			
            resultSet.close();
            statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return team_matches;
	}
}
