package hu.ppke.itk.designpatterns.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import hu.ppke.itk.designpatterns.Team;

public class JDBCTeamDAO implements TeamDAO {
	
	Connection conn = null;
	List<Team> teams = new ArrayList<>();
	
	public Connection connect() {
        try {
        		Class.forName("org.sqlite.JDBC");
        		String db = "jdbc:sqlite:database.sqlite";
            // create a connection to the database
            if(conn == null) {
            		conn = DriverManager.getConnection(db);
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
        return conn;
    }

	@Override
	public List<Team> select() {
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT team_api_id, team_long_name, team_short_name FROM Team");
			
			while(resultSet.next()) {
				int id = resultSet.getInt("team_api_id");
				String name = resultSet.getString("team_long_name");
				String short_name = resultSet.getString("team_short_name");
				Team t = new Team(id, name, short_name);
				teams.add(t);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return teams;
	}
	
	public void closeConnection(){
		 try {
			 if (conn != null) {
				 conn.close();
			 }
		 } catch (Exception e) { }
	}
	
	@Override
	public Team selectByID(int id) {
		Team t = null;
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT team_long_name, team_short_name FROM Team WHERE team_api_id=" + id);
			
			while(resultSet.next()) {
				String name = resultSet.getString("team_long_name");
				String short_name = resultSet.getString("team_short_name");
				t = new Team(id, name, short_name);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return t;
	}
	
	@Override
	public List<String> selectNames() {
		List<String> names = new ArrayList<>();
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT team_long_name FROM Team");
			
			while(resultSet.next()) {
				names.add(resultSet.getString("team_long_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return names;
	}
	
	@Override
	public Team selectByName(String long_name) {
		Team t = null;
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT team_api_id, team_long_name, team_short_name FROM Team WHERE team_long_name=\"" + long_name + "\"");
			
			while(resultSet.next()) {
				int id = resultSet.getInt("team_api_id");
				String short_name = resultSet.getString("team_short_name");
				t = new Team(id, long_name, short_name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return t;
	}

}
