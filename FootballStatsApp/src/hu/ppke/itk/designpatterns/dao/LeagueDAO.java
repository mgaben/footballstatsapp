package hu.ppke.itk.designpatterns.dao;

import java.util.List;

import hu.ppke.itk.designpatterns.business.LeagueSeasonStat;

public interface LeagueDAO {
	List<String> selectLeagueNames();
	int selectLeagueIdByName(String name);
	List<LeagueSeasonStat> selectLeagueStat();
}
