package hu.ppke.itk.designpatterns.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hu.ppke.itk.designpatterns.AttackingWorkRate;
import hu.ppke.itk.designpatterns.DefensiveWorkRate;
import hu.ppke.itk.designpatterns.Player;
import hu.ppke.itk.designpatterns.PreferredFoot;

public class JDBCPlayerDAO implements PlayerDAO {
	
	Connection conn = null;
	List<Player> players = new ArrayList<>();
	
	public Connection connect() {
        try {
        		Class.forName("org.sqlite.JDBC");
        		String db = "jdbc:sqlite:database.sqlite";
            // create a connection to the database
            if(conn == null) {
            		conn = DriverManager.getConnection(db);
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
        return conn;
    }
	
	@Override
	public List<String> selectAllNames() {
		List<String> names = new ArrayList<>();
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT player_name FROM Player");
			while(resultSet.next()) {
				names.add(resultSet.getString("player_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return names;
	}
	
	@Override
	public List<Player> select() {
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Player");
			
			while(resultSet.next()) {
				Player p = null;
				int id = resultSet.getInt("id");
				int player_id = resultSet.getInt("player_api_id");
				String name = resultSet.getString("player_name");
				String birthDateString = resultSet.getString("birthday");
				DateFormat format = new SimpleDateFormat("YYYY-MM-DD 00:00:00");
				Date birthDay = null;
				try {
					birthDay = format.parse(birthDateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				int height = resultSet.getInt("height");
				int weight = resultSet.getInt("weight");
				
				ResultSet playerAttrSet = statement.executeQuery("SELECT * FROM Player_attributes WHERE id=" + id + " ORDER BY date DESC LIMIT 1");
				
				PreferredFoot foot = PreferredFoot.valueOf(playerAttrSet.getString("preferred_foot").toUpperCase());
				AttackingWorkRate attackRate = AttackingWorkRate.valueOf(playerAttrSet.getString("attacking_work_rate").toUpperCase());
				DefensiveWorkRate defenseRate = DefensiveWorkRate.valueOf(playerAttrSet.getString("defensive_work_rate").toUpperCase());
				
				int heading_accuracy = playerAttrSet.getInt("heading_accuracy");
				int finishing = playerAttrSet.getInt("finishing");
				int volleys = playerAttrSet.getInt("volleys");
				int shot_power = playerAttrSet.getInt("shot_power");
				int long_shots = playerAttrSet.getInt("long_shots"); 
				int penalties = playerAttrSet.getInt("penalties");
				int long_passing = playerAttrSet.getInt("long_passing");
				int crossing = playerAttrSet.getInt("crossing");
				int short_passing = playerAttrSet.getInt("short_passing");
				int free_kick_accuracy = playerAttrSet.getInt("free_kick_accuracy");
				int curve = playerAttrSet.getInt("curve");
				int dribbling = playerAttrSet.getInt("dribbling");
				int ball_control = playerAttrSet.getInt("ball_control");
				int acceleration = playerAttrSet.getInt("acceleration");
				int sprint_speed = playerAttrSet.getInt("sprint_speed");
				int agility = playerAttrSet.getInt("agility");
				int reactions = playerAttrSet.getInt("reactions");
				int balance = playerAttrSet.getInt("balance");
				int jumping = playerAttrSet.getInt("jumping");
				int stamina = playerAttrSet.getInt("stamina");
				int strength = playerAttrSet.getInt("strength");
				int aggression = playerAttrSet.getInt("aggression");
				int positioning = playerAttrSet.getInt("positioning");
				int vision = playerAttrSet.getInt("vision");
				int marking = playerAttrSet.getInt("marking");
				int standing_tackle = playerAttrSet.getInt("standing_tackle");
				int sliding_tackle = playerAttrSet.getInt("sliding_tackle");
				int interceptions = playerAttrSet.getInt("interceptions");
				int gk_diving = playerAttrSet.getInt("gk_diving");
				int gk_handling = playerAttrSet.getInt("gk_handling");
				int gk_kicking = playerAttrSet.getInt("gk_kicking");
				int gk_positioning = playerAttrSet.getInt("gk_positioning");
				int gk_reflexes = playerAttrSet.getInt("gk_reflexes");
				
				playerAttrSet.close();
				p = new Player(player_id, name, birthDay, height, weight, foot,
						attackRate, defenseRate, heading_accuracy, finishing, volleys, 
						shot_power, long_shots, penalties, long_passing, crossing, short_passing, 
						free_kick_accuracy, curve, dribbling, ball_control, acceleration, sprint_speed, 
						agility, reactions, balance, jumping, stamina, strength, aggression, positioning, 
						vision, marking, standing_tackle, sliding_tackle, interceptions, 
						gk_diving, gk_handling, gk_kicking, gk_positioning, gk_reflexes);
				
				players.add(p);
			}
			
            resultSet.close();
            statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return players;
	}
	
	 public void closeConnection(){
		 try {
			 if (conn != null) {
				 conn.close();
			 }
		 } catch (Exception e) { }
	}

	@Override
	public Player selectByID(int id) {
		Player p = null;
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Player WHERE player_api_id=" + String.valueOf(id));
			
			while(resultSet.next()) {
				int player_id = resultSet.getInt("player_api_id");
				String name = resultSet.getString("player_name");
				String birthDateString = resultSet.getString("birthday");
				DateFormat format = new SimpleDateFormat("YYYY-MM-DD 00:00:00");
				Date birthDay = null;
				try {
					birthDay = format.parse(birthDateString);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				int height = resultSet.getInt("height");
				int weight = resultSet.getInt("weight");
				ResultSet playerAttrSet = statement.executeQuery("SELECT * FROM Player_attributes WHERE player_api_id=" + id);
				PreferredFoot foot = PreferredFoot.valueOf(playerAttrSet.getString("preferred_foot").toUpperCase());
				
				// get rid off anomalies in dataset
				String attack_rate = playerAttrSet.getString("attacking_work_rate");
				if(attack_rate == null) attack_rate = "null";
				
				String defense_rate = playerAttrSet.getString("defensive_work_rate");
				if(defense_rate == null) defense_rate = "null";
				switch (defense_rate) {
				case "0":
					defense_rate = "_0";
					break;
				case "9":
					defense_rate = "_9";
					break;
				case "8":
					defense_rate = "_8";
					break;
				case "7":
					defense_rate = "_7";
					break;
				case "6":
					defense_rate = "_6";
					break;
				case "5":
					defense_rate = "_5";
					break;
				case "4":
					defense_rate = "_4";
					break;
				case "3":
					defense_rate = "_3";
					break;
				case "2":
					defense_rate = "_2";
					break;
				case "1":
					defense_rate = "_1";
					break;
				default:
					break;
				}
				
				AttackingWorkRate attackRate = AttackingWorkRate.valueOf(attack_rate.toUpperCase());
				DefensiveWorkRate defenseRate = DefensiveWorkRate.valueOf(defense_rate.toUpperCase());
				
				Integer heading_accuracy = playerAttrSet.getInt("heading_accuracy");
				Integer finishing = playerAttrSet.getInt("finishing");
				Integer volleys = playerAttrSet.getInt("volleys");
				Integer shot_power = playerAttrSet.getInt("shot_power");
				Integer long_shots = playerAttrSet.getInt("long_shots"); 
				Integer penalties = playerAttrSet.getInt("penalties");
				Integer long_passing = playerAttrSet.getInt("long_passing");
				Integer crossing = playerAttrSet.getInt("crossing");
				Integer short_passing = playerAttrSet.getInt("short_passing");
				Integer free_kick_accuracy = playerAttrSet.getInt("free_kick_accuracy");
				Integer curve = playerAttrSet.getInt("curve");
				Integer dribbling = playerAttrSet.getInt("dribbling");
				Integer ball_control = playerAttrSet.getInt("ball_control");
				Integer acceleration = playerAttrSet.getInt("acceleration");
				Integer sprint_speed = playerAttrSet.getInt("sprint_speed");
				Integer agility = playerAttrSet.getInt("agility");
				Integer reactions = playerAttrSet.getInt("reactions");
				Integer balance = playerAttrSet.getInt("balance");
				Integer jumping = playerAttrSet.getInt("jumping");
				Integer stamina = playerAttrSet.getInt("stamina");
				Integer strength = playerAttrSet.getInt("strength");
				Integer aggression = playerAttrSet.getInt("aggression");
				Integer positioning = playerAttrSet.getInt("positioning");
				Integer vision = playerAttrSet.getInt("vision");
				Integer marking = playerAttrSet.getInt("marking");
				Integer standing_tackle = playerAttrSet.getInt("standing_tackle");
				Integer sliding_tackle = playerAttrSet.getInt("sliding_tackle");
				Integer interceptions = playerAttrSet.getInt("interceptions");
				Integer gk_diving = playerAttrSet.getInt("gk_diving");
				Integer gk_handling = playerAttrSet.getInt("gk_handling");
				Integer gk_kicking = playerAttrSet.getInt("gk_kicking");
				Integer gk_positioning = playerAttrSet.getInt("gk_positioning");
				Integer gk_reflexes = playerAttrSet.getInt("gk_reflexes");
				
				playerAttrSet.close();
				p = new Player(player_id, name, birthDay, height, weight, foot,
						attackRate, defenseRate, heading_accuracy, finishing, volleys, 
						shot_power, long_shots, penalties, long_passing, crossing, short_passing, 
						free_kick_accuracy, curve, dribbling, ball_control, acceleration, sprint_speed, 
						agility, reactions, balance, jumping, stamina, strength, aggression, positioning, 
						vision, marking, standing_tackle, sliding_tackle, interceptions, 
						gk_diving, gk_handling, gk_kicking, gk_positioning, gk_reflexes);
				
			}
			
            resultSet.close();
            statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return p;
	}

}
