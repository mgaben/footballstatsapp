package hu.ppke.itk.designpatterns.dao;

import java.util.List;

import hu.ppke.itk.designpatterns.Team;

public interface TeamDAO {
	public List<Team> select();
	public Team selectByID(int id);
	public List<String> selectNames();
	public Team selectByName(String long_name);
}
