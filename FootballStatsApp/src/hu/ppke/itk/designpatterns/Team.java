package hu.ppke.itk.designpatterns;

public class Team {
	private int id;
	private String name, short_name;
	
	public Team(int id, String name, String short_name) {
		super();
		this.id = id;
		this.name = name;
		this.short_name = short_name;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getShort_name() {
		return short_name;
	}
}
