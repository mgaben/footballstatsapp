package hu.ppke.itk.designpatterns.business;

public class LeagueSeasonStat {
	private String country, league, season;
	private int stages, numOfTeams, sumGoals;
	private float homeTeamGoals, awayTeamGoals, avgGoalDiff, avgGoals;
	
	public LeagueSeasonStat(String country, String league, String season, int stages, int numOfTeams, int sumGoals,
			float homeTeamGoals, float awayTeamGoals, float avgGoalDiff, float avgGoals) {
		super();
		this.country = country;
		this.league = league;
		this.season = season;
		this.stages = stages;
		this.numOfTeams = numOfTeams;
		this.sumGoals = sumGoals;
		this.homeTeamGoals = homeTeamGoals;
		this.awayTeamGoals = awayTeamGoals;
		this.avgGoalDiff = avgGoalDiff;
		this.avgGoals = avgGoals;
	}

	public String getCountry() {
		return country;
	}

	public String getLeague() {
		return league;
	}

	public String getSeason() {
		return season;
	}

	public int getStages() {
		return stages;
	}

	public int getNumOfTeams() {
		return numOfTeams;
	}

	public int getSumGoals() {
		return sumGoals;
	}

	public float getHomeTeamGoals() {
		return homeTeamGoals;
	}

	public float getAwayTeamGoals() {
		return awayTeamGoals;
	}

	public float getAvgGoalDiff() {
		return avgGoalDiff;
	}

	public float getAvgGoals() {
		return avgGoals;
	}
}
