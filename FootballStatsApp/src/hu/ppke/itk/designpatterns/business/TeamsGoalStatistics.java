package hu.ppke.itk.designpatterns.business;

import java.util.HashMap;
import java.util.List;

import hu.ppke.itk.designpatterns.Match;
import hu.ppke.itk.designpatterns.Team;
import hu.ppke.itk.designpatterns.dao.JDBCMatchDAO;
import hu.ppke.itk.designpatterns.dao.JDBCTeamDAO;

public class TeamsGoalStatistics {
	private JDBCMatchDAO matchDAO = new JDBCMatchDAO();
	private JDBCTeamDAO teamDAO = new JDBCTeamDAO();
	private List<Match> matches;
	private String team1;
	private String team2;
	private int team1_id;
	private int team2_id;
	private Team t1;
	private Team t2;
	
	public TeamsGoalStatistics(String team1, String team2) {
		this.team1 = team1;
		this.team2 = team2;
		// connect to database
		matchDAO.connect();
		teamDAO.connect();
		// get teams by their name
		t1 = teamDAO.selectByName(team1);
		t2 = teamDAO.selectByName(team2);
		matches = matchDAO.selectByOpponentsId(t1.getId(), t2.getId());
		this.team1_id = t1.getId();
		this.team2_id = t2.getId();
		matchDAO.closeConnection();
		teamDAO.closeConnection();
	}
	
	public int[] sumGoals() {
		int goals[] = new int[2]; // first column: team1, second column: team2
		goals[0] = 0;
		goals[1] = 0;
		for (int i = 0; i < matches.size(); i++) {
			if(matches.get(i).getHome_team().getId() == team1_id) {
				goals[0] += matches.get(i).getHome_team_goal();
			} else {
				goals[1] += matches.get(i).getHome_team_goal();
			}
			
			if(matches.get(i).getAway_team().getId() == team2_id) {
				goals[1] += matches.get(i).getAway_team_goal();
			} else {
				goals[0] += matches.get(i).getAway_team_goal();
			}
		}
		
		return goals;
	}
	
	public HashMap<String, Float> avgBets() {
		HashMap<String, Float> hmap = new HashMap<String, Float>();
		float B365H = 0; 
		float B365D = 0;
		float B365A = 0;
		float BWH = 0;
		float BWD = 0;
		float BWA = 0;
		float IWH = 0;
		float IWD = 0;
		float IWA = 0;
		float LBH = 0;
		float LBD = 0;
		float LBA = 0;
		float PSH = 0;
		float PSD = 0;
		float PSA = 0;
		float WHH = 0;
		float WHD = 0;
		float WHA = 0;
		float SJH = 0;
		float SJD = 0;
		float SJA = 0;
		float VCH = 0;
		float VCD = 0;
		float VCA = 0;
		float GBH = 0;
		float GBD = 0;
		float GBA = 0;
		float BSH = 0;
		float BSD = 0;
		float BSA = 0;
		for (int i = 0; i < matches.size(); ++i) {
			B365H += matches.get(i).getB365H(); 
			B365D += matches.get(i).getB365D();
			B365A += matches.get(i).getB365A();
			BWH += matches.get(i).getBWH();
			BWD += matches.get(i).getBWD();
			BWA += matches.get(i).getBWA();
			IWH += matches.get(i).getIWH();
			IWD += matches.get(i).getIWD();
			IWA += matches.get(i).getIWA();
			LBH += matches.get(i).getLBH();
			LBD += matches.get(i).getLBD();
			LBA += matches.get(i).getLBA();
			PSH += matches.get(i).getPSH();
			PSD += matches.get(i).getPSD();
			PSA += matches.get(i).getPSA();
			WHH += matches.get(i).getWHH();
			WHD += matches.get(i).getWHD();
			WHA += matches.get(i).getWHA();
			SJH += matches.get(i).getSJH();
			SJD += matches.get(i).getSJD();
			SJA += matches.get(i).getSJA();
			VCH += matches.get(i).getVCH();
			VCD += matches.get(i).getVCD();
			VCA += matches.get(i).getVCA();
			GBH += matches.get(i).getGBH();
			GBD += matches.get(i).getGBD();
			GBA += matches.get(i).getGBA();
			BSH += matches.get(i).getBSH();
			BSD += matches.get(i).getBSD();
			BSA += matches.get(i).getBSA();
		}

		hmap.put("B365H", B365H /= matches.size());
		hmap.put("B365D", B365D /= matches.size());
		hmap.put("B365A", B365A /= matches.size());
		hmap.put("BWH", BWH /= matches.size());
		hmap.put("BWD", BWD /= matches.size());
		hmap.put("BWA", BWA /= matches.size());
		hmap.put("IWH", IWH /= matches.size());
		hmap.put("IWD", IWD /= matches.size());
		hmap.put("IWA", IWA /= matches.size());
		hmap.put("LBH", LBH /= matches.size());
		hmap.put("LBD", LBD /= matches.size());
		hmap.put("LBA", LBA /= matches.size());
		hmap.put("PSH", PSH /= matches.size());
		hmap.put("PSD", PSD /= matches.size());
		hmap.put("PSA", PSA /= matches.size());
		hmap.put("WHH", WHH /= matches.size());
		hmap.put("WHD", WHD /= matches.size()); 
		hmap.put("WHA", WHA /= matches.size());
		hmap.put("SJH", SJH /= matches.size());
		hmap.put("SJD", SJD /= matches.size());
		hmap.put("SJA", SJA /= matches.size()); 
		hmap.put("VCH", VCH /= matches.size());
		hmap.put("VCD", VCD /= matches.size());
		hmap.put("VCA", VCA /= matches.size());
		hmap.put("GBH", GBH /= matches.size());
		hmap.put("GBD", GBD /= matches.size());
		hmap.put("GBA", GBA /= matches.size());
		hmap.put("BSH", BSH /= matches.size());
		hmap.put("BSD", BSD /= matches.size());
		hmap.put("BSA", BSA /= matches.size());
		
		return hmap;
	}
	
	public Team getTeam1() {
		return t1;
	}
	
	public Team getTeam2() {
		return t2;
	}
}
