package hu.ppke.itk.designpatterns.business;

import java.util.List;

import hu.ppke.itk.designpatterns.dao.JDBCLeagueDAO;

public class LeagueGoalStats {
	private JDBCLeagueDAO leagueDAO = new JDBCLeagueDAO();
	private List<LeagueSeasonStat> stats;
	
	public LeagueGoalStats(String league) {
		// connect to database
		leagueDAO.connect();
		// get season statistics
		stats = leagueDAO.selectLeagueStat();
		// close connection to database
		leagueDAO.closeConnection();
	}
	
	public List<LeagueSeasonStat> getStats() {
		return stats;
	}
}
