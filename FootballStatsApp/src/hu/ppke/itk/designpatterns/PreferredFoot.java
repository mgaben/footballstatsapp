package hu.ppke.itk.designpatterns;

public enum PreferredFoot {
	LEFT, RIGHT;
}
