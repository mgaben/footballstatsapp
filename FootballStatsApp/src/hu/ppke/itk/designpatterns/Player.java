package hu.ppke.itk.designpatterns;

import java.util.Date;

public class Player {
	private int id;
	private String name;
	private Date birthDay;
	private int height;
	private int weight;
	private PreferredFoot foot;
	private AttackingWorkRate attackRate;
	private DefensiveWorkRate defenseRate;
	// shooting skills
	private int heading_accuracy, finishing, volleys, shot_power, long_shots, penalties;
	// passing skills
	private int long_passing, crossing, short_passing;
	// ball skills
	private int free_kick_accuracy, curve, dribbling, ball_control;
	// physical attributes
	private int acceleration, sprint_speed, agility, reactions, balance, jumping, stamina, strength;
	// mental attributes
	private int aggression, positioning, vision;
	// defending skills
	private int marking, standing_tackle, sliding_tackle, interceptions;
	// goalkeeping attributes
	private int gk_diving, gk_handling, gk_kicking, gk_positioning, gk_reflexes;
	
	public Player(int id, String name, Date birthDay, int height, int weight, PreferredFoot foot,
			AttackingWorkRate attackRate, DefensiveWorkRate defenseRate, int heading_accuracy, int finishing,
			int volleys, int shot_power, int long_shots, int penalties, int long_passing, int crossing,
			int short_passing, int free_kick_accuracy, int curve, int dribbling, int ball_control, int acceleration,
			int sprint_speed, int agility, int reactions, int balance, int jumping, int stamina, int strength,
			int aggression, int positioning, int vision, int marking, int standing_tackle, int sliding_tackle,
			int interceptions, int gk_diving, int gk_handling, int gk_kicking, int gk_positioning, int gk_reflexes) {
		super();
		this.id = id;
		this.name = name;
		this.birthDay = birthDay;
		this.height = height;
		this.weight = weight;
		this.foot = foot;
		this.attackRate = attackRate;
		this.defenseRate = defenseRate;
		this.heading_accuracy = heading_accuracy;
		this.finishing = finishing;
		this.volleys = volleys;
		this.shot_power = shot_power;
		this.long_shots = long_shots;
		this.penalties = penalties;
		this.long_passing = long_passing;
		this.crossing = crossing;
		this.short_passing = short_passing;
		this.free_kick_accuracy = free_kick_accuracy;
		this.curve = curve;
		this.dribbling = dribbling;
		this.ball_control = ball_control;
		this.acceleration = acceleration;
		this.sprint_speed = sprint_speed;
		this.agility = agility;
		this.reactions = reactions;
		this.balance = balance;
		this.jumping = jumping;
		this.stamina = stamina;
		this.strength = strength;
		this.aggression = aggression;
		this.positioning = positioning;
		this.vision = vision;
		this.marking = marking;
		this.standing_tackle = standing_tackle;
		this.sliding_tackle = sliding_tackle;
		this.interceptions = interceptions;
		this.gk_diving = gk_diving;
		this.gk_handling = gk_handling;
		this.gk_kicking = gk_kicking;
		this.gk_positioning = gk_positioning;
		this.gk_reflexes = gk_reflexes;
	}
	
	// All getter for attributes
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public Date getBirthDay() {
		return birthDay;
	}
	public int getHeight() {
		return height;
	}
	public int getWeight() {
		return weight;
	}
	public PreferredFoot getFoot() {
		return foot;
	}
	public AttackingWorkRate getAttackRate() {
		return attackRate;
	}
	public DefensiveWorkRate getDefenseRate() {
		return defenseRate;
	}
	public int getHeading_accuracy() {
		return heading_accuracy;
	}
	public int getFinishing() {
		return finishing;
	}
	public int getVolleys() {
		return volleys;
	}
	public int getShot_power() {
		return shot_power;
	}
	public int getLong_shots() {
		return long_shots;
	}
	public int getPenalties() {
		return penalties;
	}
	public int getLong_passing() {
		return long_passing;
	}
	public int getCrossing() {
		return crossing;
	}
	public int getShort_passing() {
		return short_passing;
	}
	public int getFree_kick_accuracy() {
		return free_kick_accuracy;
	}
	public int getCurve() {
		return curve;
	}
	public int getDribbling() {
		return dribbling;
	}
	public int getBall_control() {
		return ball_control;
	}
	public int getAcceleration() {
		return acceleration;
	}
	public int getSprint_speed() {
		return sprint_speed;
	}
	public int getAgility() {
		return agility;
	}
	public int getReactions() {
		return reactions;
	}
	public int getBalance() {
		return balance;
	}
	public int getJumping() {
		return jumping;
	}
	public int getStamina() {
		return stamina;
	}
	public int getStrength() {
		return strength;
	}
	public int getAggression() {
		return aggression;
	}
	public int getPositioning() {
		return positioning;
	}
	public int getVision() {
		return vision;
	}
	public int getMarking() {
		return marking;
	}
	public int getStanding_tackle() {
		return standing_tackle;
	}
	public int getSliding_tackle() {
		return sliding_tackle;
	}
	public int getInterceptions() {
		return interceptions;
	}
	public int getGk_diving() {
		return gk_diving;
	}
	public int getGk_handling() {
		return gk_handling;
	}
	public int getGk_kicking() {
		return gk_kicking;
	}
	public int getGk_positioning() {
		return gk_positioning;
	}
	public int getGk_reflexes() {
		return gk_reflexes;
	}
}
