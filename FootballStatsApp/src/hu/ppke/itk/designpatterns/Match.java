package hu.ppke.itk.designpatterns;

import java.io.StringReader;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class Match {
	// where
	private String country, league;
	// when
	private Date date;
	private String season;
	private int stage;
	// teams
	private Team home_team, away_team;
	// goals
	private int home_team_goal, away_team_goal;
	// lineups
	private List<Integer> home_player_X;
	private List<Integer> home_player_Y;
	private List<Integer> away_player_X;
	private List<Integer> away_player_Y;
	// players
	private List<Player> home_players;
	private List<Player> away_players;
	// match stats
	private String goal, shoton, shotoff, foulcommit, card, cross, corner, possession;
	// odds
	private float B365H, B365D, B365A, BWH, BWD, BWA, IWH, IWD, IWA,	LBH, LBD, LBA, PSH,
		PSD, PSA, WHH, WHD, WHA, SJH, SJD, SJA, VCH, VCD, VCA, GBH, GBD, GBA, BSH, BSD, BSA;
	
	public Match(String country, String league, String season, int stage, Date date, Team home_team,
			Team away_team, int home_team_goal, int away_team_goal, List<Integer> home_player_X, List<Integer> home_player_Y,
			List<Integer> away_player_X, List<Integer> away_player_Y, List<Player> home_players, List<Player> away_players, String goal, 
			String shoton, String shotoff, String foulcommit, String card, String cross, String corner, String possession, float b365h, float b365d, float b365a,
			float bWH, float bWD, float bWA, float iWH, float iWD, float iWA, float lBH, float lBD, float lBA,
			float pSH, float pSD, float pSA, float wHH, float wHD, float wHA, float sJH, float sJD, float sJA,
			float vCH, float vCD, float vCA, float gBH, float gBD, float gBA, float bSH, float bSD,
			float bSA) {
		super();
		this.country = country;
		this.league = league;
		this.season = season;
		this.stage = stage;
		this.date = date;
		this.home_team = home_team;
		this.away_team = away_team;
		this.home_team_goal = home_team_goal;
		this.away_team_goal = away_team_goal;
		
		this.home_player_X = home_player_X;
		this.home_player_Y = home_player_Y;
		this.away_player_X = away_player_X;
		this.away_player_Y = away_player_Y;
		
		this.home_players = home_players;
		this.away_players = away_players;
		
		this.goal = goal;
		this.shoton = shoton;
		this.shotoff = shotoff;
		this.foulcommit = foulcommit;
		this.card = card;
		this.cross = cross;
		this.corner = corner;
		this.possession = possession;
		B365H = b365h;
		B365D = b365d;
		B365A = b365a;
		BWH = bWH;
		BWD = bWD;
		BWA = bWA;
		IWH = iWH;
		IWD = iWD;
		IWA = iWA;
		LBH = lBH;
		LBD = lBD;
		LBA = lBA;
		PSH = pSH;
		PSD = pSD;
		PSA = pSA;
		WHH = wHH;
		WHD = wHD;
		WHA = wHA;
		SJH = sJH;
		SJD = sJD;
		SJA = sJA;
		VCH = vCH;
		VCD = vCD;
		VCA = vCA;
		GBH = gBH;
		GBD = gBD;
		GBA = gBA;
		BSH = bSH;
		BSD = bSD;
		BSA = bSA;
	}
	public String getCountry() {
		return country;
	}
	public String getLeague() {
		return league;
	}
	public String getSeason() {
		return season;
	}
	public int getStage() {
		return stage;
	}
	public Date getDate() {
		return date;
	}
	public Team getHome_team() {
		return home_team;
	}
	public Team getAway_team() {
		return away_team;
	}
	public int getHome_team_goal() {
		return home_team_goal;
	}
	public int getAway_team_goal() {
		return away_team_goal;
	}
	public String getGoal() {
		return goal;
	}
	public String getShoton() {
		return shoton;
	}
	public String getShotoff() {
		return shotoff;
	}
	public String getFoulcommit() {
		return foulcommit;
	}
	public String getCard() {
		return card;
	}
	public String getCross() {
		return cross;
	}
	public String getCorner() {
		return corner;
	}
	public String getPossession() {
		return possession;
	}
	public float getB365H() {
		return B365H;
	}
	public float getB365D() {
		return B365D;
	}
	public float getB365A() {
		return B365A;
	}
	public float getBWH() {
		return BWH;
	}
	public float getBWD() {
		return BWD;
	}
	public float getBWA() {
		return BWA;
	}
	public float getIWH() {
		return IWH;
	}
	public float getIWD() {
		return IWD;
	}
	public float getIWA() {
		return IWA;
	}
	public float getLBH() {
		return LBH;
	}
	public float getLBD() {
		return LBD;
	}
	public float getLBA() {
		return LBA;
	}
	public float getPSH() {
		return PSH;
	}
	public float getPSD() {
		return PSD;
	}
	public float getPSA() {
		return PSA;
	}
	public float getWHH() {
		return WHH;
	}
	public float getWHD() {
		return WHD;
	}
	public float getWHA() {
		return WHA;
	}
	public float getSJH() {
		return SJH;
	}
	public float getSJD() {
		return SJD;
	}
	public float getSJA() {
		return SJA;
	}
	public float getVCH() {
		return VCH;
	}
	public float getVCD() {
		return VCD;
	}
	public float getVCA() {
		return VCA;
	}
	public float getGBH() {
		return GBH;
	}
	public float getGBD() {
		return GBD;
	}
	public float getGBA() {
		return GBA;
	}
	public float getBSH() {
		return BSH;
	}
	public float getBSD() {
		return BSD;
	}
	public float getBSA() {
		return BSA;
	}
	public List<Integer> getHome_player_X() {
		return home_player_X;
	}
	public List<Integer> getHome_player_Y() {
		return home_player_Y;
	}
	public List<Integer> getAway_player_X() {
		return away_player_X;
	}
	public List<Integer> getAway_player_Y() {
		return away_player_Y;
	}
	public List<Player> getHome_players() {
		return home_players;
	}
	public List<Player> getAway_players() {
		return away_players;
	}
	
	// Create XML document from string instead of a file
	public Document loadXMLFromString(String xml) throws Exception
	{
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(xml));
	    return builder.parse(is);
	}
}
