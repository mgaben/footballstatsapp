package hu.ppke.itk.designpatterns.gui;

public interface Observer {
	public void update();
}