package hu.ppke.itk.designpatterns.gui;

import java.util.LinkedList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;

public class ToggleButtonSubject {
	protected LinkedList<Observer> observers;
	protected ToggleGroup group;
	protected LinkedList<RadioButton> radioButtons;
	protected String value;
	
	public void notifiyObservers() {
		for(Observer o : observers) {
			o.update();
		}
	}
	
	public ToggleButtonSubject(List<String> buttons) {
		observers = new LinkedList<>();
		group = new ToggleGroup();
		radioButtons = new LinkedList<>();
		for (String string : buttons) {
			RadioButton b = new RadioButton(string);
			radioButtons.add(b);
			b.setToggleGroup(group);
		}
		
		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				RadioButton b = (RadioButton)newValue.getToggleGroup().getSelectedToggle();
				value = b.getText();
				notifiyObservers();
			}
		});
	}
	
	public void attach(Observer obs) {
		observers.add(obs);
	}
	
	public void detach(Observer obs) {
		observers.remove(obs);
	}
	
	public void notifyObservers()
	{
		for(Observer obs : observers) {
			obs.update();
		}
	}
	
	public ToggleGroup getGroup() {
		return this.group;
	}
	
	public LinkedList<RadioButton> getRadioButtons() {
		return this.radioButtons;
	}
	
	public String getValue() {
		return this.value;
	}
}
