package hu.ppke.itk.designpatterns.gui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hu.ppke.itk.designpatterns.Match;
import hu.ppke.itk.designpatterns.business.LeagueSeasonStat;
import hu.ppke.itk.designpatterns.business.TeamsGoalStatistics;
import hu.ppke.itk.designpatterns.dao.JDBCLeagueDAO;
import hu.ppke.itk.designpatterns.dao.JDBCMatchDAO;
import hu.ppke.itk.designpatterns.dao.JDBCTeamDAO;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GUI extends Application {
	
	private TabPane layout = new TabPane();
	// Tabs for different statistics
	private Tab leagueTab = new Tab("League Statistics");
	private Tab matchTab = new Tab("Face-to-Face Match Statistics");
	private Tab lineupTab = new Tab("LineUps");
	
	private Label B365H_label = new Label();
	private Label B365D_label = new Label();
	private Label B365A_label = new Label();
	private Label BWH_label = new Label();
	private Label BWD_label = new Label();
	private Label BWA_label = new Label();
	private Label IWH_label = new Label();
	private Label IWD_label = new Label();
	private Label IWA_label = new Label();
	private Label LBH_label = new Label();
	private Label LBD_label = new Label();
	private Label LBA_label = new Label();
	private Label PSH_label = new Label();
	private Label PSD_label = new Label();
	private Label PSA_label = new Label();
	private Label WHH_label = new Label();
	private Label WHD_label = new Label();
	private Label WHA_label = new Label();
	private Label SJH_label = new Label();
	private Label SJD_label = new Label();
	private Label SJA_label = new Label();
	private Label VCH_label = new Label();
	private Label VCD_label = new Label();
	private Label VCA_label = new Label();
	private Label GBH_label = new Label();
	private Label GBD_label = new Label();
	private Label GBA_label = new Label();
	private Label BSH_label = new Label();
	private Label BSD_label = new Label();
	private Label BSA_label = new Label();
	
	private List<Match> matches = new ArrayList<>();
	
	private ChartObserver chartObserver;
	private ToggleButtonSubject tButtonSubject;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		JDBCLeagueDAO leagueDAO = new JDBCLeagueDAO();
		JDBCTeamDAO teamDAO = new JDBCTeamDAO();
		JDBCMatchDAO matchDAO = new JDBCMatchDAO();
		leagueDAO.connect();
		teamDAO.connect();
		matchDAO.connect();
		// ****** LEAGUE TAB ******
		
		List<String> leagues = leagueDAO.selectLeagueNames();
		List<LeagueSeasonStat> stats = leagueDAO.selectLeagueStat();
		
		BorderPane leaguePane = new BorderPane();
		
		// add button names to construct toggle button set
		List<String> buttons = new ArrayList<>();
		buttons.add("Average Goals per Game");
		buttons.add("Average Goal Difference");
		buttons.add("Total Goals");
		List<LeagueSeasonStat> seasonStat = leagueDAO.selectLeagueStat();
		tButtonSubject = new ToggleButtonSubject(buttons);
		chartObserver = new ChartObserver(tButtonSubject, seasonStat, leaguePane);
		tButtonSubject.attach(chartObserver);

		VBox toggles = new VBox();
		toggles.getChildren().addAll(tButtonSubject.getRadioButtons());
		toggles.setSpacing(5);
		leaguePane.setLeft(toggles);
		
		leagueTab.setContent(leaguePane);
		leagueTab.setClosable(false);
		
		// ****** MATCH STATISTIC TAB ******
		BorderPane matchStatPane = new BorderPane();
		HBox teams = new HBox();
		VBox rightStats = new VBox();
		// get all team names
		List<String> allTeamNames = teamDAO.selectNames();
		// create textfields
		AutoCompleteTextField team1 = new AutoCompleteTextField();
		team1.getEntries().addAll(allTeamNames);
		AutoCompleteTextField team2 = new AutoCompleteTextField();
		team2.getEntries().addAll(allTeamNames);
		Label l1 = new Label("versus");
		Button searchButton = new Button("Search");
		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(team1.getText().compareTo("") == 0 || team2.getText().compareTo("") == 0) 
					return;
				
				TeamsGoalStatistics goalStat = new TeamsGoalStatistics(team1.getText(), team2.getText());
				int goals[] = goalStat.sumGoals();
				CategoryAxis xAxis = new CategoryAxis();
				NumberAxis yAxis = new NumberAxis();
				BarChart<String, Number> barChart = new BarChart<>(xAxis, yAxis);
				barChart.setCategoryGap(2);
				barChart.setBarGap(0);
				xAxis.setLabel("Teams");       
		        yAxis.setLabel("Goals");
		        XYChart.Series series1 = new XYChart.Series();
		        series1.setName("Goal Statistics");       
		        series1.getData().add(new XYChart.Data(goalStat.getTeam1().getName(), goals[0]));
		        series1.getData().add(new XYChart.Data(goalStat.getTeam2().getName(), goals[1]));
		        barChart.getData().addAll(series1);
		        matchStatPane.setCenter(barChart);
		        
		        HashMap<String, Float> hmap = goalStat.avgBets();
		        	B365H_label.setText("B365H: " + String.valueOf(hmap.get("B365H")));
        			B365D_label.setText("B365D: " + String.valueOf(hmap.get("B365D")));
        			B365A_label.setText("B365A: " + String.valueOf(hmap.get("B365A")));
			    	BWH_label.setText("BWH: " + String.valueOf(hmap.get("BWH")));
			    	BWD_label.setText("BWD: " + String.valueOf(hmap.get("BWD")));
			    	BWA_label.setText("BWA: " + String.valueOf(hmap.get("BWA")));
			    	IWH_label.setText("IWH: " + String.valueOf(hmap.get("IWH")));
			    	IWD_label.setText("IWD: " + String.valueOf(hmap.get("IWD")));
			    	IWA_label.setText("IWA: " + String.valueOf(hmap.get("IWA")));
			    	LBH_label.setText("LBH: " + String.valueOf(hmap.get("LBH")));
			    	LBD_label.setText("LBD: " + String.valueOf(hmap.get("LBD")));
			    	LBA_label.setText("LBA: " + String.valueOf(hmap.get("LBA")));
			    	PSH_label.setText("PSH: " + String.valueOf(hmap.get("PSH")));
			    	PSD_label.setText("PSD: " + String.valueOf(hmap.get("PSD")));
			    	PSA_label.setText("PSA: " + String.valueOf(hmap.get("PSA")));
			    	WHH_label.setText("WHH: " + String.valueOf(hmap.get("WHH")));
			    	WHD_label.setText("WHD: " + String.valueOf(hmap.get("WHD")));
			    	WHA_label.setText("WHA: " + String.valueOf(hmap.get("WHA")));
			    	SJH_label.setText("SJH: " + String.valueOf(hmap.get("SJH")));
			    	SJD_label.setText("SJD: " + String.valueOf(hmap.get("SJD")));
			    	SJA_label.setText("SJA: " + String.valueOf(hmap.get("SJA")));
			    	VCH_label.setText("VCH: " + String.valueOf(hmap.get("VCH")));
			    	VCD_label.setText("VCD: " + String.valueOf(hmap.get("VCD")));
			    	VCA_label.setText("VCA: " + String.valueOf(hmap.get("VCA")));
			    	GBH_label.setText("GBH: " + String.valueOf(hmap.get("GBH")));
			    	GBD_label.setText("GBD: " + String.valueOf(hmap.get("GBD")));
			    	GBA_label.setText("GBA: " + String.valueOf(hmap.get("GBA")));
			    	BSH_label.setText("BSH: " + String.valueOf(hmap.get("BSH")));
			    	BSD_label.setText("BSD: " + String.valueOf(hmap.get("BSD")));
			    	BSA_label.setText("BSA: " + String.valueOf(hmap.get("BSA")));
		        rightStats.getChildren().addAll(B365H_label, B365D_label, B365A_label, BWH_label, BWD_label,
		        		BWA_label, IWH_label, IWD_label, IWA_label, LBH_label, LBD_label, LBA_label, PSH_label, PSD_label,
		        		PSA_label, WHH_label, WHD_label, WHA_label, SJH_label, SJD_label, SJA_label, VCH_label, VCD_label,
		        		VCA_label, GBH_label, GBD_label, GBA_label, BSH_label, BSD_label, BSA_label);
				//rightStats.setSpacing(10);
				matchStatPane.setRight(rightStats);
			}
		});
		teams.getChildren().addAll(team1, l1, team2, searchButton);
		teams.setSpacing(10);
		teams.setAlignment(Pos.CENTER);
		matchStatPane.setTop(teams);
		matchTab.setContent(matchStatPane);
		matchTab.setClosable(false);
		
		// ****** LINEUP TAB ******
		BorderPane lineupPane = new BorderPane();
		// create textfields
		AutoCompleteTextField lineup_team1 = new AutoCompleteTextField();
		lineup_team1.getEntries().addAll(allTeamNames);
		AutoCompleteTextField lineup_team2 = new AutoCompleteTextField();
		lineup_team2.getEntries().addAll(allTeamNames);
		Label l2 = new Label("versus");
		
		ListView<String> listOfMatches = new ListView<String>();
		Button searchLineupButton = new Button("Search Match");
		searchLineupButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(lineup_team1.getText().compareTo("") == 0 || lineup_team2.getText().compareTo("") == 0) 
					return;
				
				JDBCMatchDAO matchDAO = new JDBCMatchDAO();
				matchDAO.connect();
				//List<Match> 
				matches = matchDAO.selectByTeams(lineup_team1.getText(), lineup_team2.getText());
				List<String> match_list = new ArrayList<>();
				for(int i=0; i<matches.size(); ++i) {
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String date = df.format(matches.get(i).getDate());
					match_list.add(matches.get(i).getHome_team().getName() + " - " + matches.get(i).getAway_team().getName() + " " + matches.get(i).getHome_team_goal() + "-" + matches.get(i).getAway_team_goal() + " (" + date + ")");
				}
				listOfMatches.getItems().addAll(match_list);
			}
		});
		
		listOfMatches.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				Match m = matches.get(listOfMatches.getSelectionModel().getSelectedIndex());
				List<Integer> home_X = m.getHome_player_X();
				List<Integer> home_Y = m.getHome_player_Y();
				List<Integer> away_X = m.getAway_player_X();
				List<Integer> away_Y = m.getAway_player_Y();
				// Defining the x,y axis               
				NumberAxis xAxis = new NumberAxis(0, 10, 1);         
				NumberAxis yAxis = new NumberAxis(0, 22, 1);
				// Creating the Scatter chart 
				ScatterChart<String, Number> scatterChart = new ScatterChart(xAxis, yAxis);
				// Prepare XYChart.Series objects by setting data 
			    XYChart.Series home = new XYChart.Series();
			    XYChart.Series away = new XYChart.Series();
			    home.getData().add(new XYChart.Data(5, 0.5));
	    			away.getData().add(new XYChart.Data(5, 21.5));
			    for(int i=1; i<11; ++i) {
			    		home.getData().add(new XYChart.Data(home_X.get(i), home_Y.get(i)));
			    		away.getData().add(new XYChart.Data(away_X.get(i), 22-away_Y.get(i)));
			    }
			    
			    home.setName(lineup_team1.getText());
			    away.setName(lineup_team2.getText());
			    // Setting the data to scatter chart        
			    scatterChart.getData().addAll(home, away);
			    
			    lineupPane.setCenter(scatterChart);
			}
		});
		HBox select_lineup = new HBox();
		select_lineup.getChildren().addAll(lineup_team1, l2, lineup_team2, searchLineupButton);
		select_lineup.setAlignment(Pos.CENTER);
		select_lineup.setSpacing(10);
		lineupPane.setTop(select_lineup);
		lineupPane.setLeft(listOfMatches);
	    
	    lineupTab.setContent(lineupPane);
	    lineupTab.setClosable(false);
		
		// last steps
	    matchDAO.closeConnection();
	    teamDAO.closeConnection();
		leagueDAO.closeConnection();
		
		layout.getTabs().add(leagueTab);
		layout.getTabs().add(matchTab);
		layout.getTabs().add(lineupTab);
		
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Football Statistics Application");
		primaryStage.setWidth(1024);
		primaryStage.setHeight(700);
		primaryStage.show();
	}

}