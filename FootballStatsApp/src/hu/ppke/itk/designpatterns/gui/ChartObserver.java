package hu.ppke.itk.designpatterns.gui;

import java.util.ArrayList;
import java.util.List;

import hu.ppke.itk.designpatterns.business.LeagueSeasonStat;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.BorderPane;

public class ChartObserver implements Observer {
	
	protected ToggleButtonSubject tButtonSubject;
	protected List<LeagueSeasonStat> stats;
	
	protected BorderPane leaguePane;
    
    protected XYChart.Series spainSeries;
    protected XYChart.Series germanySeries;
    protected XYChart.Series franceSeries;
    protected XYChart.Series italySeries;
    protected XYChart.Series englandSeries;
    
    protected List<LeagueSeasonStat> spain = new ArrayList<>();
    protected List<LeagueSeasonStat> germany = new ArrayList<>();
    protected List<LeagueSeasonStat> france = new ArrayList<>();
    protected List<LeagueSeasonStat> italy = new ArrayList<>();
    protected List<LeagueSeasonStat> england = new ArrayList<>();
	
	public ChartObserver(ToggleButtonSubject tButtonSubject, List<LeagueSeasonStat> stats, BorderPane leaguePane) {
		this.tButtonSubject = tButtonSubject;
		this.stats = stats;

		this.spainSeries = new XYChart.Series<>();
		this.germanySeries = new XYChart.Series<>();
		this.franceSeries = new XYChart.Series<>();
		this.italySeries = new XYChart.Series<>();
		this.englandSeries = new XYChart.Series<>();
		
		this.spainSeries.setName("Spain");
		this.germanySeries.setName("Germany");
        this.franceSeries.setName("France");
        this.italySeries.setName("Italy");
        this.englandSeries.setName("England");
		
		this.leaguePane = leaguePane;
				
		for(int i=0; i<stats.size(); ++i) {
			// 'Spain', 'Germany', 'France', 'Italy', 'England'
			String country = stats.get(i).getCountry();
			if(country.equals("Spain")) 
				spain.add(stats.get(i));
			else if(country.equals("Germany"))
				germany.add(stats.get(i));
			else if(country.equals("France"))
				france.add(stats.get(i));
			else if(country.equals("Italy"))
				italy.add(stats.get(i));
			else if(country.equals("England"))
				england.add(stats.get(i));				
		}
	}

	@Override
	public void update() {		
		spainSeries.getData().clear();
		germanySeries.getData().clear();
		franceSeries.getData().clear();
		italySeries.getData().clear();
		englandSeries.getData().clear();
		
		CategoryAxis xAxis;
		NumberAxis yAxis;
		xAxis = new CategoryAxis();
		yAxis = new NumberAxis();
	    LineChart<String,Number> lineChart = new LineChart<String,Number>(xAxis, yAxis);
	    lineChart.setCreateSymbols(false);
	    
	    double max = 0;
	    
		switch (tButtonSubject.getValue()) {
		case "Average Goals per Game":
			lineChart.setTitle(tButtonSubject.getValue());
			for(int i=0; i<spain.size(); ++i) {
				spainSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), spain.get(i).getAvgGoals()));
				germanySeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), germany.get(i).getAvgGoals()));
				franceSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), france.get(i).getAvgGoals()));
				italySeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), italy.get(i).getAvgGoals()));
				englandSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), england.get(i).getAvgGoals()));
			}
			break;
			
		case "Average Goal Difference":
			lineChart.setTitle(tButtonSubject.getValue());
			for(int i=0; i<spain.size(); ++i) {
				spainSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), spain.get(i).getAvgGoalDiff()));
				germanySeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), germany.get(i).getAvgGoalDiff()));
				franceSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), france.get(i).getAvgGoalDiff()));
				italySeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), italy.get(i).getAvgGoalDiff()));
				englandSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), england.get(i).getAvgGoalDiff()));
			}
			break;
			
		case "Total Goals":
			lineChart.setTitle(tButtonSubject.getValue());
			for(int i=0; i<spain.size(); ++i) {
				spainSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), spain.get(i).getSumGoals()));
				germanySeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), germany.get(i).getSumGoals()));
				franceSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), france.get(i).getSumGoals()));
				italySeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), italy.get(i).getSumGoals()));
				englandSeries.getData().add(new XYChart.Data(spain.get(i).getSeason(), england.get(i).getSumGoals()));
			}
			break;
			
		default:
			break;
		}

	    lineChart.getData().addAll(spainSeries, germanySeries, franceSeries, italySeries, englandSeries);
	    this.leaguePane.setCenter(lineChart);
	}

}
